#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <grp.h>
void main()
{
  pid_t x[4];
 
  x[0] = x[1] = 1;
  if (getgroups(1,x) == 0) if (setgroups(1,x) == -1) _exit(1);
 
  if (getgroups(1,x) == -1) _exit(1);
  if (x[1] != 1) _exit(1);
  x[1] = 2;
  if (getgroups(1,x) == -1) _exit(1);
  if (x[1] != 2) _exit(1);
  _exit(0);
}
